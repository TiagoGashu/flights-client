export class Plane {
	id: number;
	brand: string;
	model: string;
}