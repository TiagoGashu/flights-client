import { City } from './city'
import { Plane } from './plane'
import { Pilot } from './pilot'

export class Flight {
  id: number;
	
  departureDate: Date;
  arrivalDate: Date;

  departureTime: Date;
  arrivalTime: Date;
	
  departureCity: City;
  arrivalCity: City;

  flightNumber: string;

  flightStatus: string;

  plane: Plane;
  pilot: Pilot
  
}