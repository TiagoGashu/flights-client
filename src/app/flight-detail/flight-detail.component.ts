import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';

import { Flight } from '../model/flight'

import { FlightService } from '../service/flight.service'

@Component({
  selector: 'app-flight-detail',
  templateUrl: './flight-detail.component.html',
  styleUrls: ['./flight-detail.component.css']
})
export class FlightDetailComponent implements OnInit {

  flight: Flight;

  constructor(
    private flightService: FlightService,
    private route: ActivatedRoute,
    private location: Location
  ) { }

  ngOnInit() {
    this.getFlight();
  }

  getFlight(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.flightService.getFlight(id).subscribe(flight => this.flight = flight);
  }

  goBack(): void {
    this.location.back();
  }

}
