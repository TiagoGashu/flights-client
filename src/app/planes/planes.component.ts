import { Component, OnInit } from '@angular/core';
import {NgForm} from '@angular/forms';

import { Plane } from '../model/plane'

import { PlaneService } from '../service/plane.service'

@Component({
  selector: 'app-planes',
  templateUrl: './planes.component.html',
  styleUrls: ['./planes.component.css']
})
export class PlanesComponent implements OnInit {

  planes: Plane[];

  selectedPlane: Plane;

  newPlaneBrand: string;
  newPlaneModel: string;

  constructor(
    private planeService: PlaneService
  ) { }

  ngOnInit() {
    this.getPlanes();
  }

  getPlanes(): void {
    this.planeService.getPlanes().subscribe(planes => {
      this.planes = planes;
    });
  }

  addPlane(): void {
    if(!this.newPlaneBrand || this.newPlaneBrand.trim() === '' 
      || !this.newPlaneModel || this.newPlaneModel.trim() === '') {
      // TODO: validation msg
      return;
    }
    var brand = this.newPlaneBrand.trim();
    var model = this.newPlaneModel.trim();

    this.planeService.addPlane({ brand, model } as Plane)
      .subscribe(plane => {
        this.planes.push(plane);
      });

    this.newPlaneBrand = '';
    this.newPlaneModel = '';
  }

  editPlane(Plane: Plane): void {
    this.selectedPlane = Plane;
  }

  savePlane(plane: Plane): void {
    this.planeService.updatePlane(plane)
      .subscribe(() => {
        this.goBack();
      })
  }

  goBack(): void {
    this.selectedPlane = null;
  }

  deletePlane(plane: Plane): void {
    this.planes = this.planes.filter((c) => { return c.id !== plane.id });
    this.planeService.deletePlane(plane).subscribe()
  }

}
