import { Injectable } from '@angular/core';

import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';

import { Pilot } from '../model/pilot';

import { IAutocomplete } from './iautocomplete'

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class PilotService implements IAutocomplete<Pilot> {

  private pilotsUrl = 'http://localhost:8080/api/pilots';

  constructor(
    private http: HttpClient
  ) {}

  getPilots(): Observable<Pilot[]> {
    return this.http.get<Pilot[]>(this.pilotsUrl)
      .pipe(
        tap(pilots => this.log(`fetched pilots`)),
        catchError(this.handleError('getPilots', []))
      );
  }

  addPilot(pilot: Pilot): Observable<Pilot> {
    return this.http.post<Pilot>(this.pilotsUrl, pilot, httpOptions)
      .pipe(
        tap(cities => this.log(`add new pilot`)),
        catchError(this.handleError<Pilot>('addPilot'))
      );
  }

  updatePilot(pilot: Pilot): Observable<Pilot> {
    return this.http.put(this.pilotsUrl, pilot, httpOptions)
      .pipe(
        tap(_ => this.log(`updated pilot id:{pilot.id}`)),
        catchError(this.handleError<any>('updatePilot'))
      );
  }

  deletePilot(pilot: Pilot): Observable<Pilot> {
    var deleteUrl = `${this.pilotsUrl}/${pilot.id}`
    return this.http.delete<Pilot>(deleteUrl, httpOptions)
      .pipe(
        tap(_ => this.log(`deleted pilot id:{pilot.id}`)),
        catchError(this.handleError<Pilot>('deletePilot'))
      );
  }

  autocomplete(pilotName: string): Observable<Pilot[]> {
    return this.http.get<Pilot[]>(`${this.pilotsUrl}/?name=${pilotName}`)
      .pipe(
        tap(cities => this.log(`fetched pilots with name like ${pilotName}`)),
        catchError(this.handleError('search', []))
      )
  }

  // private functions

  private log(message: string): void {
    console.log('PilotService: ' + message);
  }

  /**
	 * Handle Http operation that failed.
	 * Let the app continue.
	 * @param operation - name of the operation that failed
	 * @param result - optional value to return as the observable result
	 */
	private handleError<T> (operation = 'operation', result?: T) {
	  return (error: any): Observable<T> => {
	    console.error(error); // log to console instead
	    this.log(`${operation} failed: ${error.message}`);
	    return of(result as T);
	  };
	}

}
