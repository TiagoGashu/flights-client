import { TestBed, inject } from '@angular/core/testing';
import { HttpClientModule } from '@angular/common/http';

import { PilotService } from './pilot.service';

describe('PilotService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ HttpClientModule ],
      providers: [ PilotService ]
    });
  });

  it('should be created', inject([PilotService], (service: PilotService) => {
    expect(service).toBeTruthy();
  }));
});
