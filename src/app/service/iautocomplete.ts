import { Observable } from 'rxjs/Observable';

export interface IAutocomplete<T> {
	
	autocomplete(term: string): Observable<T[]>

}