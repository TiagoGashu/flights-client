import { TestBed, inject } from '@angular/core/testing';
import { HttpClientModule } from '@angular/common/http';

import { PlaneService } from './plane.service';

describe('PlaneService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ HttpClientModule ],
      providers: [ PlaneService ]
    });
  });

  it('should be created', inject([PlaneService], (service: PlaneService) => {
    expect(service).toBeTruthy();
  }));
});
