import { Injectable } from '@angular/core';

import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';

import { City } from '../model/city';
import { IAutocomplete } from '../service/iautocomplete'

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class CityService implements IAutocomplete<City>{

  private citiesUrl = 'http://localhost:8080/api/cities';

  constructor(
    private http: HttpClient
  ) {}

  getCities(): Observable<City[]> {
    return this.http.get<City[]>(this.citiesUrl)
      .pipe(
        tap(cities => this.log(`fetched cities`)),
        catchError(this.handleError('getCities', []))
      );
  }

  addCity(city: City): Observable<City> {
    return this.http.post<City>(this.citiesUrl, city, httpOptions)
      .pipe(
        tap(cities => this.log(`add new city`)),
        catchError(this.handleError<City>('addCity'))
      );
  }

  updateCity(city: City): Observable<City> {
    return this.http.put(this.citiesUrl, city, httpOptions)
      .pipe(
        tap(_ => this.log(`updated city id:{city.id}`)),
        catchError(this.handleError<any>('updateCity'))
      );
  }

  deleteCity(city: City): Observable<City> {
    var deleteUrl = `${this.citiesUrl}/${city.id}`
    return this.http.delete<City>(deleteUrl, httpOptions)
      .pipe(
        tap(_ => this.log(`deleted city id:{city.id}`)),
        catchError(this.handleError<City>('deleteCity'))
      );
  }

  autocomplete(cityName: string): Observable<City[]> {
    return this.http.get<City[]>(`${this.citiesUrl}/?name=${cityName}`)
      .pipe(
        tap(cities => this.log(`fetched cities with name like ${cityName}`)),
        catchError(this.handleError('serachCities', []))
      )
  }

  // private functions

  private log(message: string): void {
    console.log('CityService: ' + message);
  }

  /**
	 * Handle Http operation that failed.
	 * Let the app continue.
	 * @param operation - name of the operation that failed
	 * @param result - optional value to return as the observable result
	 */
	private handleError<T> (operation = 'operation', result?: T) {
	  return (error: any): Observable<T> => {
	    console.error(error); // log to console instead
	    this.log(`${operation} failed: ${error.message}`);
	    return of(result as T);
	  };
	}

}
