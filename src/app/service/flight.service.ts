import { Injectable } from '@angular/core';

import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';

import { Flight } from '../model/flight';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';

import * as moment from 'moment';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class FlightService {

  private flightsUrl = 'http://localhost:8080/api/flights';

  STATUS:string[] = [
    'SCHEDULED', 
    'DELAYED', 
    'DEPARTED', 
    'LANDED', 
    'LANDED_DELAYED', 
    'CANCELED'
  ]

  constructor(
    private http: HttpClient
  ) {}

  getFlight(id: number): Observable<Flight> {
    const url = `${this.flightsUrl}/${id}`;
    return this.http.get<Flight>(url).pipe(
      tap(_ => this.log(`fetched flight id=${id}`)),
      catchError(this.handleError<Flight>(`getFlight id=${id}`))
    );
  }

  getFlights(): Observable<Flight[]> {
    return this.http.get<Flight[]>(this.flightsUrl)
      .pipe(
        tap(Flights => this.log(`fetched flights`)),
        catchError(this.handleError('getFlights', []))
      );
  }

  addFlight(flight: Flight): Observable<Flight> {
    this.adjustFlightToSave(flight);
    return this.http.post<Flight>(this.flightsUrl, flight, httpOptions)
      .pipe(
        tap(flights => this.log(`add new flight`)),
        catchError(this.handleError<Flight>('addFlight'))
      );
  }

  updateFlight(flight: Flight): Observable<Flight> {
    this.adjustFlightToSave(flight);
    return this.http.put(this.flightsUrl, flight, httpOptions)
      .pipe(
        tap(_ => this.log(`updated flight id:{flight.id}`)),
        catchError(this.handleError<any>('updateFlight'))
      );
  }

  searchFlights(searchParams: any): Observable<Flight[]> {
    var paramsArr = [];
    for(var prop in searchParams) {
      if(searchParams[prop]) {
        if(prop == 'departureDate' || prop == 'arrivalDate') {
          var dateMoment = moment(searchParams[prop])
          dateMoment.hours(0);
          var hoursToSubtract = (new Date(dateMoment.format()).getTimezoneOffset()) / 60;
          dateMoment.subtract(hoursToSubtract, 'hours')
          searchParams[prop] = dateMoment.toJSON();
        }
        paramsArr.push(`${prop}=${searchParams[prop]}`);
      }
    }
    var finalParams = paramsArr.join('&');

    var searchUrl = `${this.flightsUrl}/?${finalParams}`
    return this.http.get<Flight[]>(searchUrl)
      .pipe(
        tap(Flights => this.log(`fetched flights`)),
        catchError(this.handleError('searchFlights', []))
      );
  }

  private datePropsNames = ['departure', 'arrival']
  private auxTimeProp = 'Time';
  private entityProp = 'Date';

  adjustFlightToEdit(flight: Flight): void {
    this.datePropsNames.forEach((propName) => {
      if(typeof(flight[propName + this.entityProp]) == 'string' ) {
        flight[propName + this.entityProp] = new Date(flight[propName + this.entityProp]);
        flight[propName + this.auxTimeProp] = new Date(flight[propName + this.entityProp]);
      } else if(flight.departureDate.toDateString){
        flight[propName + this.auxTimeProp] = new Date(flight[propName + this.entityProp]);
      }
    })
  }

  // private functions

  private adjustFlightToSave(flight: Flight): void {
    this.datePropsNames.forEach((propName) => {
      if(typeof(flight[propName + this.entityProp]) == 'string' ) {
        var dateMoment = moment(flight[propName + this.entityProp]);
        if(flight[propName + this.auxTimeProp]) {
          dateMoment.hours(flight[propName + this.auxTimeProp].getHours());
          dateMoment.minutes(flight[propName + this.auxTimeProp].getMinutes());
          var hoursToSubtract = (new Date(dateMoment.format()).getTimezoneOffset()) / 60;
          dateMoment.subtract(hoursToSubtract, 'hours')
          flight[propName + this.entityProp] = dateMoment.toJSON();
          flight[propName + this.auxTimeProp] = null;
        }
      } else if(flight[propName + this.entityProp].toDateString) {
          var dateMoment = moment(flight[propName + this.entityProp]);
          dateMoment.hours(flight[propName + this.auxTimeProp].getHours());
          dateMoment.minutes(flight[propName + this.auxTimeProp].getMinutes());
          var hoursToSubtract = (new Date(dateMoment.format()).getTimezoneOffset()) / 60;
          dateMoment.subtract(hoursToSubtract, 'hours')
          flight[propName + this.entityProp] = dateMoment.toJSON();
      }
    })

  }

  private log(message: string): void {
    console.log('FlightService: ' + message);
  }

  /**
	 * Handle Http operation that failed.
	 * Let the app continue.
	 * @param operation - name of the operation that failed
	 * @param result - optional value to return as the observable result
	 */
	private handleError<T> (operation = 'operation', result?: T) {
	  return (error: any): Observable<T> => {
	    console.error(error); // log to console instead
	    this.log(`${operation} failed: ${error.message}`);
	    return of(result as T);
	  };
	}

}
