import { Injectable } from '@angular/core';

import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';

import { Plane } from '../model/plane';
import { IAutocomplete } from './iautocomplete'

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class PlaneService implements IAutocomplete<Plane> {

  private planesUrl = 'http://localhost:8080/api/planes';

  constructor(
    private http: HttpClient
  ) {}

  getPlanes(): Observable<Plane[]> {
    return this.http.get<Plane[]>(this.planesUrl)
      .pipe(
        tap(Planes => this.log(`fetched planes`)),
        catchError(this.handleError('getPlanes', []))
      );
  }

  addPlane(plane: Plane): Observable<Plane> {
    return this.http.post<Plane>(this.planesUrl, plane, httpOptions)
      .pipe(
        tap(planes => this.log(`add new plane`)),
        catchError(this.handleError<Plane>('addPlane'))
      );
  }

  updatePlane(plane: Plane): Observable<Plane> {
    return this.http.put(this.planesUrl, plane, httpOptions)
      .pipe(
        tap(_ => this.log(`updated plane id:{plane.id}`)),
        catchError(this.handleError<any>('updatePlane'))
      );
  }

  deletePlane(plane: Plane): Observable<Plane> {
    var deleteUrl = `${this.planesUrl}/${plane.id}`
    return this.http.delete<Plane>(deleteUrl, httpOptions)
      .pipe(
        tap(_ => this.log(`deleted plane id:{plane.id}`)),
        catchError(this.handleError<Plane>('deletePlane'))
      );
  }

  autocomplete(planeModel: string): Observable<Plane[]> {
    return this.http.get<Plane[]>(`${this.planesUrl}/?model=${planeModel}`)
      .pipe(
        tap(cities => this.log(`fetched planes with model like ${planeModel}`)),
        catchError(this.handleError('search', []))
      )
  }

  // private functions

  private log(message: string): void {
    console.log('PlaneService: ' + message);
  }

  /**
	 * Handle Http operation that failed.
	 * Let the app continue.
	 * @param operation - name of the operation that failed
	 * @param result - optional value to return as the observable result
	 */
	private handleError<T> (operation = 'operation', result?: T) {
	  return (error: any): Observable<T> => {
	    console.error(error); // log to console instead
	    this.log(`${operation} failed: ${error.message}`);
	    return of(result as T);
	  };
	}

}
