import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { TimepickerModule } from 'ngx-bootstrap';
import { RouterTestingModule } from '@angular/router/testing';

import { FlightsComponent } from './flights.component';
import { AutocompleteSelectComponent } from '../autocomplete-select/autocomplete-select.component'

import { CityService } from '../service/city.service'
import { PilotService } from '../service/pilot.service'
import { PlaneService } from '../service/plane.service'
import { FlightService } from '../service/flight.service'

describe('FlightsComponent', () => {
  let component: FlightsComponent;
  let fixture: ComponentFixture<FlightsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FlightsComponent, AutocompleteSelectComponent ],
      imports: [ FormsModule, HttpClientModule, TimepickerModule, RouterTestingModule ],
      providers: [ CityService, PilotService, PlaneService, FlightService ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FlightsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
