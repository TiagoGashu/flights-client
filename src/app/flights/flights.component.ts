import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';

import { City } from '../model/city'
import { Pilot } from '../model/pilot'
import { Plane } from '../model/plane'
import { Flight } from '../model/flight'

import { CityService } from '../service/city.service'
import { PilotService } from '../service/pilot.service'
import { PlaneService } from '../service/plane.service'
import { FlightService } from '../service/flight.service'

@Component({
  selector: 'app-flights',
  templateUrl: './flights.component.html',
  styleUrls: ['./flights.component.css']
})
export class FlightsComponent implements OnInit {
  
  @Input() flights: Flight[];
  @Output() flightsChange: EventEmitter<Flight[]>;

  selectedFlightId: number;
  selectedFlight: Flight;

  constructor(
    private cityService: CityService,
    private pilotService: PilotService,
    private planeService: PlaneService,
    private flightService: FlightService,
    private router: Router
  ) {
    this.flightsChange = new EventEmitter();
  }

  ngOnInit() {
    if(!this.flights) {
      this.getFlights();
    }
  }

  getFlights(): void {
    this.flightService.getFlights().subscribe(flights => {
      this.flights = flights;
    });
  }

  editFlight(flight: Flight): void {
    this.flightService.getFlight(flight.id)
      .subscribe((flight) => {
        this.selectedFlight = flight;
        this.flightService.adjustFlightToEdit(this.selectedFlight)
      })
  }

  updateFlight(flight: Flight): void {
    this.flightService.updateFlight(flight)
      .subscribe(() => {
        this.goBack();
      })
  }

  cancelEdit(): void {
    this.selectedFlight = null;
  }

  goBack(): void {
    this.flights = null;
    this.flightsChange.emit(this.flights);
  }

  showFlightDetail(flight: Flight): void {
    this.router.navigate([`/flightDetail/${flight.id}`])
  }

}
