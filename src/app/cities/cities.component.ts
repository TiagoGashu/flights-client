import { Component, OnInit } from '@angular/core';
import {NgForm} from '@angular/forms';

import { City } from '../model/city'

import { CityService } from '../service/city.service'

@Component({
  selector: 'app-cities',
  templateUrl: './cities.component.html',
  styleUrls: ['./cities.component.css']
})
export class CitiesComponent implements OnInit {

  cities: City[];

  selectedCity: City;

  newCityName: string;

  constructor(
    private cityService: CityService
  ) {}

  ngOnInit() {
    this.getCities();
  }

  getCities(): void {
    this.cityService.getCities().subscribe(cities => {
      this.cities = cities;
    });
  }

  addCity(): void {
    if(!this.newCityName || this.newCityName.trim() === '') {
      // TODO: validation msg
      return;
    }
    var name = this.newCityName.trim();
    this.cityService.addCity({ name } as City)
      .subscribe(city => {
        this.cities.push(city);
      });
    this.newCityName = '';
  }

  editCity(city: City): void {
    this.selectedCity = city;
  }

  saveCity(city: City): void {
    this.cityService.updateCity(city)
      .subscribe(() => {
        this.goBack();
      })
  }

  goBack(): void {
    this.selectedCity = null;
  }

  deleteCity(city: City): void {
    this.cities = this.cities.filter((c) => { return c.id !== city.id });
    this.cityService.deleteCity(city).subscribe()
  }

}
