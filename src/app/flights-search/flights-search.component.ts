import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { City } from '../model/city'
import { Flight } from '../model/flight'

import { CityService } from '../service/city.service'
import { FlightService } from '../service/flight.service'

class SearchType {
  name: string;
  active: boolean;

  constructor(name: string) {
    this.name = name;
    this.active = false;
  }
}

@Component({
  selector: 'app-flights-search',
  templateUrl: './flights-search.component.html',
  styleUrls: ['./flights-search.component.css']
})
export class FlightsSearchComponent implements OnInit {

  isListingFlights: boolean;
  _flights: Flight[];

  searchTypes: SearchType[];
  selectedType: SearchType;

  flightNumber: string;
  flightStatus: string;

  departureDate: Date;
  arrivalDate: Date;

  departureCity: City;
  arrivalCity: City;

  constructor(
    private route: ActivatedRoute,
    private cityService: CityService,
    private flightService: FlightService
  ) { }

  ngOnInit() {
    this.isListingFlights = false;
    this.searchTypes = [
      new SearchType('flight'),
      new SearchType('route')
    ]
  }

  setActive(type: SearchType): void {
    this.searchTypes.forEach((type) => type.active = false);
    this.selectedType = type;
    type.active = true;
    if(type.name == 'route') {
      this.flightNumber = null;
    } else {
      this.departureCity = null;
      this.arrivalCity = null;
      this.arrivalDate = null;
    }
  }

  searchFlights(): void {
    var searchParams:any = {};
    searchParams.flightNumber = this.flightNumber;
    searchParams.flightStatus = this.flightStatus;
    searchParams.departureDate = this.departureDate;
    searchParams.arrivalDate = this.arrivalDate;
    searchParams.departureCityId = this.departureCity;
    searchParams.arrivalCityID = this.arrivalCity;

    this.flightService.searchFlights(searchParams)
      .subscribe((flights) => { 
        this._flights = flights;
        this.isListingFlights = true;
      });
  }

  get flights(): Flight[] {
    return this._flights;
  }

  set flights(flights: Flight[]) {
    if(!flights) {
      this.isListingFlights = false;
    } else {
      this._flights = flights;
    }
  }

}
