import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterTestingModule } from '@angular/router/testing';
import { TimepickerModule } from 'ngx-bootstrap';

import { IAutocomplete } from '../service/iautocomplete'

import { FlightsSearchComponent } from './flights-search.component';
import { FlightsComponent } from '../flights/flights.component';
import { AutocompleteSelectComponent } from '../autocomplete-select/autocomplete-select.component'

import { CityService } from '../service/city.service'
import { FlightService } from '../service/flight.service'

import { DbMockService } from '../db-mock.service';

describe('FlightsSearchComponent', () => {
  let component: FlightsSearchComponent;
  let fixture: ComponentFixture<FlightsSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FlightsSearchComponent, FlightsComponent, AutocompleteSelectComponent ],
      imports: [ FormsModule, HttpClientModule, RouterTestingModule, TimepickerModule ],
      providers: [ CityService, FlightService ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FlightsSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
