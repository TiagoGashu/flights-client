import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';

import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/observable/of';

import {
   debounceTime, distinctUntilChanged, switchMap
 } from 'rxjs/operators';

import { IAutocomplete } from '../service/iautocomplete'

@Component({
  selector: 'app-autocomplete-select',
  templateUrl: './autocomplete-select.component.html',
  styleUrls: ['./autocomplete-select.component.css']
})
export class AutocompleteSelectComponent<T> implements OnInit {

  @Input() service: IAutocomplete<T>;
  @Input() optionLabelProp: string;
  @Input() selected: T;
  @Output() selectedChange: EventEmitter<T>;
  @Input() placeholder: string;

  searchTerms: Subject<string>;
  term: string;
  options$: Observable<T[]>;

  visible: boolean;

  constructor() {
    this.visible = false;
    this.searchTerms = new Subject<string>();
    this.selectedChange = new EventEmitter();
  }

  ngOnInit() {
    this.initSubject();
  }

  initSubject(): void {
    this.options$ = this.searchTerms.pipe(
      debounceTime(300),
      distinctUntilChanged(),
      switchMap((term: string) => this.service.autocomplete(term))
    );
  }

  toggleShowAutocomplete(): void {
    this.visible = !this.visible;
  }

  select(option: T): void {
    this.selected = option;
    this.initSubject();
    this.visible = false;
    this.term = null;
    this.selectedChange.emit(this.selected);
  }

  remove(): void {
    this.selected = null;
    this.initSubject();
    this.visible = false;
    this.term = null;
    this.selectedChange.emit(this.selected);
  }

  search(term: string): void {
    this.searchTerms.next(term);
  }

}
