import { TestBed, inject } from '@angular/core/testing';

import { DbMockService } from './db-mock.service';

describe('DbMockService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DbMockService]
    });
  });

  it('should be created', inject([DbMockService], (service: DbMockService) => {
    expect(service).toBeTruthy();
  }));
});
