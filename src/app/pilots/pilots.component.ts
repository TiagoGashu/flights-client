import { Component, OnInit } from '@angular/core';
import {NgForm} from '@angular/forms';

import { Pilot } from '../model/pilot'

import { PilotService } from '../service/pilot.service'

@Component({
  selector: 'app-pilots',
  templateUrl: './pilots.component.html',
  styleUrls: ['./pilots.component.css']
})
export class PilotsComponent implements OnInit {

  pilots: Pilot[];

  selectedPilot: Pilot;

  newPilotName: string;

  constructor(
    private pilotService: PilotService
  ) { }

  ngOnInit() {
    this.getPilots();
  }

  getPilots(): void {
    this.pilotService.getPilots().subscribe(pilots => {
      this.pilots = pilots;
    });
  }

  addPilot(): void {
    if(!this.newPilotName || this.newPilotName.trim() === '') {
      // TODO: validation msg
      return;
    }
    var name = this.newPilotName.trim();
    this.pilotService.addPilot({ name } as Pilot)
      .subscribe(pilot => {
        this.pilots.push(pilot);
      });
    this.newPilotName = '';
  }

  editPilot(pilot: Pilot): void {
    this.selectedPilot = pilot;
  }

  savePilot(pilot: Pilot): void {
    this.pilotService.updatePilot(pilot)
      .subscribe(() => {
        this.goBack();
      })
  }

  goBack(): void {
    this.selectedPilot = null;
  }

  deletePilot(pilot: Pilot): void {
    this.pilots = this.pilots.filter((c) => { return c.id !== pilot.id });
    this.pilotService.deletePilot(pilot).subscribe()
  }

}
