import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

// http
import { HttpClientModule } from '@angular/common/http';
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
// db mock
import { DbMockService }  from './db-mock.service';

// providers
import { CityService } from './service/city.service'
import { PilotService } from './service/pilot.service'
import { PlaneService } from './service/plane.service'
import { FlightService } from './service/flight.service'

import { AppComponent } from './app.component'
import { CitiesComponent } from './cities/cities.component'
import { PilotsComponent } from './pilots/pilots.component'
import { PlanesComponent } from './planes/planes.component'
import { FlightsComponent } from './flights/flights.component'
import { FlightsSearchComponent } from './flights-search/flights-search.component';
import { FlightComponent } from './flight/flight.component';

import { AppRoutingModule } from './app-routing.module'

import { AutocompleteSelectComponent } from './autocomplete-select/autocomplete-select.component'

import { TimepickerModule } from 'ngx-bootstrap/timepicker';
import { FlightDetailComponent } from './flight-detail/flight-detail.component';

@NgModule({
  declarations: [
    AppComponent,
    CitiesComponent,
    PilotsComponent,
    PlanesComponent,
    FlightsComponent,
    AutocompleteSelectComponent,
    FlightsSearchComponent,
    FlightComponent,
    FlightDetailComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    //HttpClientInMemoryWebApiModule.forRoot(
    //  DbMockService, { dataEncapsulation: false }
    //),
    AppRoutingModule,
    TimepickerModule.forRoot()
  ],
  providers: [
    CityService,
    PilotService,
    PlaneService,
    FlightService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
