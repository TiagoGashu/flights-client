import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

interface IRoute {
	routerLink: string;
	name: string;
	active: boolean;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';

  routes: IRoute[] = [
    { routerLink: '/cities', name: 'Cities',  active: false},
    { routerLink: '/pilots', name: 'Pilots', active: false},
    { routerLink: '/planes', name: 'Planes', active: false},
    { routerLink: '/flightsSearch', name: 'Search Flights', active: false},
    { routerLink: '/flight', name: 'Flight', active: false}
  ]

  constructor(
    private router: Router
  ) {}

  ngOnInit() {
    this.router.navigate([''])
    this.routes[3].active = true;
  }

  setActive(route: IRoute): void {
    this.routes.forEach((r) => r.active = false)
    route.active = true;
  }

}
