import { InMemoryDbService } from 'angular-in-memory-web-api';

import { City } from './model/city'
import { Plane } from './model/plane'
import { Pilot } from './model/pilot'
import { Flight } from './model/flight'

export class DbMockService implements InMemoryDbService {
  createDb() {
    
    // cities
    let guarulhos = {
      id: 1, name: 'Guarulhos'
    }

    let beloHorizonte = {
      id: 2, name: 'Belo Horizonte'
    }

    let portoAlegre = {
      id: 3, name: 'Porto Alegre'
    }

    const cities = [
      guarulhos, beloHorizonte, portoAlegre
    ];

    // pilots
    let jose = {
      id: 1, name: 'José da Silva'
    }

    let joao = {
      id: 2, name: 'João Oliveira'
    }

    const pilots = [
      jose, joao
    ]
    
    // planes
    let a320 = {
      id: 1, model: 'A320', brand: 'Airbus'
    }

    let boeing757 = {
      id: 2, model: '757', brand: 'Boeing'
    }

    const planes = [
      a320, boeing757
    ]

    var flight1:Flight = {
      id: 1,
      flightNumber: 'QWE0001',
      flightStatus: 'LANDED',
      departureDate: new Date(),
      departureTime: new Date(),
      arrivalDate: new Date(),
      arrivalTime: new Date(),
      departureCity: guarulhos,
      arrivalCity: beloHorizonte,
      plane: a320,
      pilot: joao
    }

    var flight2:Flight = {
      id: 2,
      flightNumber: 'ZXC0010',
      flightStatus: 'CANCELED',
      departureDate: new Date(),
      departureTime: new Date(),
      arrivalDate: new Date(),
      arrivalTime: new Date(),
      departureCity: beloHorizonte,
      arrivalCity: portoAlegre,
      plane: boeing757,
      pilot: jose
    }

    // flights
    const flights = [
      flight1, flight2
    ]

    return {
      cities,
      pilots,
      planes,
      flights
    };
  }

}
