import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';

import { City } from '../model/city'
import { Pilot } from '../model/pilot'
import { Plane } from '../model/plane'
import { Flight } from '../model/flight'

import { CityService } from '../service/city.service'
import { PilotService } from '../service/pilot.service'
import { PlaneService } from '../service/plane.service'
import { FlightService } from '../service/flight.service'

@Component({
  selector: 'app-flight',
  templateUrl: './flight.component.html',
  styleUrls: ['./flight.component.css']
})
export class FlightComponent implements OnInit {

  flight: Flight;

  constructor(
    private cityService: CityService,
    private pilotService: PilotService,
  	private planeService: PlaneService,
    private flightService: FlightService,
    private router: Router,
    private route: ActivatedRoute,
    private location: Location
  ){}

  ngOnInit() {
    this.getFlight();
  }

  getFlight(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    if(!id) {
      this.flight = new Flight();
    } else {
      this.flightService.getFlight(id).subscribe(flight => {
        this.flightService.adjustFlightToEdit(flight);
        this.flight = flight;
      })
    }
  }

  saveFlight(): void {
    if(!this.flight.flightStatus || this.flight.flightStatus.trim() === '' 
      || !this.flight.departureDate || !this.flight.arrivalDate 
      || !this.flight.departureTime || !this.flight.arrivalTime 
      || !this.flight.flightNumber || this.flight.flightNumber.trim() === '' 
      || !this.flight.departureCity || !this.flight.arrivalCity || !this.flight.plane 
      || !this.flight.pilot) {
      // TODO: validation msg
      return;
    }

    if(!this.flight.id) {
      this.flightService.addFlight(this.flight)
        .subscribe(_ => {
          this.location.back();
        });
    } else {
      this.flightService.updateFlight(this.flight)
        .subscribe(_ => {
          this.location.back();
        });
    }

    this.flight.flightStatus = null;
    this.flight.departureDate = null;
    this.flight.arrivalDate = null;
    this.flight.flightNumber = '';
    this.flight.departureCity = null;
    this.flight.arrivalCity = null;
    this.flight.plane = null;
    this.flight.pilot = null;
  }

  goBack():void {
    this.location.back();
  }

}
