import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { TimepickerModule } from 'ngx-bootstrap';
import { RouterTestingModule } from '@angular/router/testing';

import { FlightComponent } from './flight.component';
import { AutocompleteSelectComponent } from '../autocomplete-select/autocomplete-select.component'

import { CityService } from '../service/city.service'
import { PilotService } from '../service/pilot.service'
import { PlaneService } from '../service/plane.service'
import { FlightService } from '../service/flight.service'

describe('FlightComponent', () => {
  let component: FlightComponent;
  let fixture: ComponentFixture<FlightComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FlightComponent, AutocompleteSelectComponent ],
      imports: [ FormsModule, TimepickerModule, HttpClientModule, RouterTestingModule ],
      providers: [ CityService, PilotService, PlaneService, FlightService ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FlightComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
