import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CitiesComponent } from './cities/cities.component'
import { PilotsComponent } from './pilots/pilots.component'
import { PlanesComponent } from './planes/planes.component'
import { FlightsComponent } from './flights/flights.component'
import { FlightsSearchComponent} from './flights-search/flights-search.component'
import { FlightComponent } from './flight/flight.component';
import { FlightDetailComponent } from './flight-detail/flight-detail.component';

const routes: Routes = [
  { path: '', component: FlightsSearchComponent },
  { path: 'cities', component: CitiesComponent },
  { path: 'pilots', component: PilotsComponent },
  { path: 'planes', component: PlanesComponent },
  { path: 'flights', component: FlightsComponent },
  { path: 'flightsSearch', component: FlightsSearchComponent},
  { path: 'flight', component: FlightComponent},
  { path: 'flight/:id', component: FlightComponent},
  { path: 'flightDetail/:id', component: FlightDetailComponent}
];

@NgModule({
	imports: [ RouterModule.forRoot(routes) ],
	exports: [RouterModule]
})

export class AppRoutingModule { }